package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.10.v20211216-rNA", date="2023-03-21T23:08:49")
@StaticMetamodel(Routes.class)
public class Routes_ { 

    public static volatile SingularAttribute<Routes, String> travelTime;
    public static volatile SingularAttribute<Routes, Float> price;
    public static volatile SingularAttribute<Routes, Float> length;
    public static volatile SingularAttribute<Routes, Integer> cityIdOut;
    public static volatile SingularAttribute<Routes, Integer> del;
    public static volatile SingularAttribute<Routes, Integer> id;
    public static volatile SingularAttribute<Routes, Integer> cityIdIn;

}