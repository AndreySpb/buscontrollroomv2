/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package session;

import entity.Buses;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andrey
 */
@TransactionManagement(TransactionManagementType.CONTAINER)
@Stateless
public class BusManager {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @PersistenceContext(unitName = "BusControllRoomV2PU")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer addBus(String nomer, String name, Integer qntPlace , Integer status) {
            try{
                Buses bus= newBus(nomer, name, qntPlace, status);
                return 0;
            }catch (Exception e){
                context.setRollbackOnly();
                e.printStackTrace();
                return 1;
            }
    }
    
    private Buses newBus(String nomer, String name, Integer qntPlace , Integer status) {
        Buses bus = new Buses();
        bus.setName(name);
        bus.setNomer(nomer);
        bus.setQntPlace(qntPlace);
        bus.setStatus(status);
        em.persist(bus);
        return bus;
    }
    
}
