<%-- 
    Document   : add_raice
    Created on : 7 мар. 2023 г., 17:34:19
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <!-- Broadcums -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb active">
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/">Главная</a></li>
        <li class="breadcrumb-item"><a href="raices">Рейсы</a></li>
        <li class="breadcrumb-item active" aria-current="page">Новый рейс</li>
      </ol>
    </nav>
    <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Новый рейс</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form method="post" action="register_new_trip">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputRout">Маршрут</label>
                            <select id="inputRout" class="form-control" name="routeId">
                                <option selected>Выбрать...</option>
                                <c:forEach var="route" items="${routes}">
                                    <option value="${route.id}" <c:if test="${route.id eq trip.routeId}">selected</c:if>>${route.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputDtRaice">Дaта отправления</label>
                            <input type="date" class="form-control" id="inputDtRaice" name="dtStr">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputTimeRaice">Время отправления</label>
                            <input type="time" class="form-control" id="inputTimeRaice" name="timeStr">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputBus">Автобус</label>
                             <select id="inputBus" class="form-control" name="busId">
                                <option selected>Выбрать...</option>
                                <c:forEach var="bus" items="${buses}">
                                    <option value="${bus.id}" <c:if test="${bus.id eq trip.busId}">selected</c:if>>${bus.nomer} : ${bus.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputDriver">Водитель</label>
                            <select id="inputDriver" class="form-control" name="driverId">
                                <option selected>Выбрать...</option>
                                <c:forEach var="driver" items="${drivers}">
                                    <option value="${driver.id}" <c:if test="${driver.id eq trip.driverId}">selected</c:if>>${driver.family} ${driver.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="raices" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>