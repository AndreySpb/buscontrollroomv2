<%-- 
    Document   : add_ticket
    Created on : 7 мар. 2023 г., 14:38:37
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
      <!-- Broadcums -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Главная</a></li>
          <li class="breadcrumb-item"><a href="tickets">Билеты</a></li>
          <li class="breadcrumb-item active" aria-current="page">Продажа</li>
        </ol>
      </nav>
      <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Продажа билета на рейс №${trip.tripId}</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form method="post" action="register_new_ticket">
                    <div class="form-row">
                        <input type="hidden" value="${trip.tripId}" name="tripId">
                        <div class="form-group col-md-6">
                            <label for="inputFio">Маршрут</label>
                            <select id="inputStatus" class="form-control" name="routeId">
                                <option selected>Выбрать...</option>
                                <c:forEach var="route" items="${routes}">
                                    <option value="${route.id}" <c:if test="${route.id eq trip.routeId}">selected</c:if>>${route.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputdtRout">Дата отправления</label>
                            <input type="date" class="form-control" id="inputdtRout" value="${trip.dtStr}" name="dtStr">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSurname">Cумма</label>
                        <input type="text" class="form-control" id="inputSurname" value="${trip.price}" name="price">
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="tickets" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>