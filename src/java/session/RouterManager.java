/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package session;

import entity.Routes;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andrey
 */
@TransactionManagement(TransactionManagementType.CONTAINER)
@Stateless
public class RouterManager {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @PersistenceContext(unitName = "BusControllRoomV2PU")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer addRoute(Integer city_id_in, Integer city_id_out, String travel_time, Float length , Float price) {
            try{
                Routes route = newRoute(city_id_in, city_id_out, travel_time, length , price);
                return 0;
            }catch (Exception e){
                context.setRollbackOnly();
                e.printStackTrace();
                return 1;
            }
    }
    
    private Routes newRoute(Integer city_id_in, Integer city_id_out, String travel_time, Float length , Float price) {
        Routes route = new Routes();
        route.setCityIdIn(city_id_in);
        route.setCityIdOut(city_id_out);
        route.setTravelTime(travel_time);
        route.setLength(length);
        route.setPrice(price);
        em.persist(route);
        return route;
    }
}
