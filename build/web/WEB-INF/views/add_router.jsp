<%-- 
    Document   : add_router
    Created on : 7 мар. 2023 г., 15:15:42
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

    <!-- Broadcums -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb active">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/routers.jsp">Маршруты</a></li>
        <li class="breadcrumb-item active" aria-current="page">Новый маршрут</li>
      </ol>
    </nav>
    <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Новый маршрут</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form method="post" action="register_new_route">
                    <div class="form-row">
                        <input type="hidden" class="form-control" id="idRow" name="idRow" value="${route.id}">
                        <div class="form-group col-md-6">
                            <label for="inputOutPoint">Точка отправления</label>
                            <select id="inputOutPoint" class="form-control" name="outPoint">
                                <option selected>Выбрать...</option>
                                <c:forEach var="city" items="${cities}">
                                    <option value="${city.id}" <c:if test="${city.id eq route.cityIdOut}">selected</c:if>>${city.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputInPoint">Точка прибытия</label>
                            <select id="inputInPoint" class="form-control" name="inPoint">
                                <option selected>Выбрать...</option>
                                <c:forEach var="city" items="${cities}">
                                    <option value="${city.id}" <c:if test="${city.id eq route.cityIdIn}">selected</c:if>>${city.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputTimeRout">Время в пути</label>
                            <input type="text" class="form-control" id="inputTimeRout" name="travel_time" value="${route.travelTime}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputLength">Расстояние</label>
                            <input type="text" class="form-control" id="inputLength" name="length" value="${route.length}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputPrice">Стоимость</label>
                            <input type="text" class="form-control" id="inputPrice" name="price" value="${route.price}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="routers" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>