<%-- 
    Document   : add_bus
    Created on : 7 мар. 2023 г., 13:53:48
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%> 

    <!-- Broadcums -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb active">
        <li class="breadcrumb-item"><a href="/">Главная</a></li>
        <li class="breadcrumb-item"><a href="buses">Автобусы</a></li>
        <li class="breadcrumb-item active" aria-current="page">Новый автобус</li>
      </ol>
    </nav>
    <!-- ./EndBroadcums -->
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Добавить автобус</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form method="POST" 
                      <c:choose>
                        <c:when test="${bus.id gt 0}">        
                          action="update_bus" 
                        </c:when>    
                        <c:otherwise>        
                          action="register_new_bus"    
                        </c:otherwise>
                      </c:choose>
                          >
                    <input type="hidden" class="form-control" id="id" name="id" value="${bus.id}">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputNomer">Номер</label>
                        <input type="text" class="form-control" id="inputNomer" name="nomer" value="${bus.nomer}">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputName">Марка</label>
                        <input type="text" class="form-control" id="inputName" name="name" value="${bus.name}">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputQntPlace">Количество мест</label>
                      <input type="text" class="form-control" id="inputQntPlace" name="qntPlace" value="${bus.qntPlace}">
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputStatus">Статус</label>
                        <select id="inputStatus" class="form-control" name="status">
                          <option selected>Выбрать...</option>
                          <option value="1" <c:if test="${bus.status eq 1}">selected</c:if>>Работате</option>
                          <option value="2" <c:if test="${bus.status eq 2}">selected</c:if>>Простаивает</option>
                          <option value="3" <c:if test="${bus.status eq 3}">selected</c:if>>Сломан</option>
                          <option value="4" <c:if test="${bus.status eq 4}">selected</c:if>>Списан</option>

                        </select>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/BusStationControlRoom/buses.jsp" class="btn btn-secondary">Назад</a>
        </main>
    </div>