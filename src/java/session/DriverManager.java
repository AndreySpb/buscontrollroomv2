/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package session;

import entity.Drivers;
import static java.lang.System.out;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andrey
 */
@TransactionManagement(TransactionManagementType.CONTAINER)

@Stateless
public class DriverManager {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName = "BusControllRoomV2PU")
    private EntityManager em;
    
    @Resource
    private SessionContext context;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integer addDriver(final String family, final String name, final String surname, final String birthday, final Integer status) {
            try{
                Drivers driver= newDriver(family, name, surname, birthday, status);
                return 0;
            }catch (Exception e){
                context.setRollbackOnly();
                e.printStackTrace();
                return 1;
            }
    }
    
    private Drivers newDriver(String family, String name, String surname, String birthday, Integer status) {
        Drivers driver = new Drivers();
        driver.setFamily(family);
        driver.setName(name);
        driver.setSurname(surname);
        driver.setBirthday(birthday);
        driver.setStatus(status);
        em.persist(driver);
        return driver;
    }
}
