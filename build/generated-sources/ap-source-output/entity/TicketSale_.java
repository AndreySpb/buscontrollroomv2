package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.7.10.v20211216-rNA", date="2023-03-21T23:08:49")
@StaticMetamodel(TicketSale.class)
public class TicketSale_ { 

    public static volatile SingularAttribute<TicketSale, Float> price;
    public static volatile SingularAttribute<TicketSale, String> dtSale;
    public static volatile SingularAttribute<TicketSale, Integer> tripId;
    public static volatile SingularAttribute<TicketSale, Integer> id;

}