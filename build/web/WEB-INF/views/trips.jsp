<%-- 
    Document   : raices
    Created on : 7 мар. 2023 г., 12:19:59
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
      <!-- Broadcums -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/BusStationControlRoom/">Главная</a></li>
          <li class="breadcrumb-item"><a href="#">Рейсы</a></li>
          <!--li class="breadcrumb-item active" aria-current="page">Данные</li-->
        </ol>
      </nav>
      <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Список рейсов</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                  <div class="btn-group mr-2">
                    <a type="button" class="btn btn-sm btn-outline-secondary" href="/BusStationControlRoom/add_raice.jsp">Добавить</a>
                    <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                  </div>
                  <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                    This week
                  </button>
                </div>
            </div>
      
            <div class="table-responsive">
                <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Маршрут</th>
                                <th></th>
                                <th>Время отправления</th>
                                <th>Время прибытия</th>
                                <th>Автобус</th>
                                <th>Водитель</th>                         
                                <th>Всего мест</th>
                                <th>Продано</th>
                                <th>Свободно</th>
                                <th>Действие</th>
                              </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="trip" items="${trips}">
                                    <tr>
                                      <td>${trip.id}</td>
                                      <td>${trip.tripName}</td>
                                      <td>${trip.dtStr}</td>
                                      <td>${trip.timeStr}</td>
                                      <td>${trip.timeEnd}</td>
                                      <td><a href="bus?id=${trip.busId}" class="ml-1">${trip.bus}</a></td>
                                      <td><a href="driver?id=${trip.driverId}" class="ml-1">${trip.driver}</a></td>
                                      <td>${trip.qntPlace}</td>
                                      <td>${trip.qntSales}</td>
                                      <td>${trip.qntFree}</td>
                                      <td>
                                          <c:if test = "${trip.qntFree gt 0}">
                                          <a href="add_ticket?id=${trip.id}" class="ml-1">
                                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill-add" viewBox="0 0 16 16">
                                                <path d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7Zm.5-5v1h1a.5.5 0 0 1 0 1h-1v1a.5.5 0 0 1-1 0v-1h-1a.5.5 0 0 1 0-1h1v-1a.5.5 0 0 1 1 0Zm-2-6a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"/>
                                                <path d="M2 13c0 1 1 1 1 1h5.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.544-3.393C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4Z"/>
                                              </svg>
                                          </a>
                                          /
                                          </c:if>
                                          <a href="trip?id=${trip.id}" class="ml-1">
                                              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                              </svg>
                                          </a>
                                      </td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>
                    </div>
            </div>
        </main>
    </div>